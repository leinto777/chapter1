// Otniel Kevin Abiel
// 111202013015@mhs.dinus.ac.id

// melakukan import module versi ES lama (menggunakan require)
let readline = require("readline");

// input output JavaScript
let rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

// function input mengembalikan nilai promise
function input(question) {
  return new Promise((resolve) => {
    rl.question(question, (data) => {
      return resolve(data);
    });
  });
}

// method penjumlah bilangan
function tambah(x, y) {
  return x + y;
}

// method pengurangan bilangan
function kurang(x, y) {
  return x - y;
}

// method perkalian bilangan
function kali(x, y) {
  return x * y;
}

// method pembagian bilangan
function bagi(x, y) {
  return x / y;
}

// method menghitung akar kuadrat bilangan
function akarKuadrat(x) {
  return Math.sqrt(x);
}

// method menghitung luas persegi
function luasPersegi(s) {
  return s ** 2;
}

// method menghitung volume kubus
function volumeKubus(r) {
  return r ** 3;
}

// method menghitung volume tabung
function volumeTabung(r, t) {
  return Math.PI * r ** 2 * t;
}

// main function melakukan operasi inputan user
async function main() {
  try {
    let hasil = 0;
    // melakukan perulangan terus menerus dan akan berhenti bila user input angka 9
    while (true) {
      console.info(`
        1. Tambah
        2. Kurang
        3. Kali
        4. Bagi
        5. Akar Kuadrat
        6. Luas Persegi
        7. Volume Kubus
        8. Volume Tabung
        9. Out
        `);

      // user input pemilihan operasi
      let choice = await input("Masukkan pilihan :");

      if (!choice) {
        // jika choice null / undefined maka user diminta kembali melakukan input
        console.info("Harap pilih salah satu operator!");
      } else if (choice == 1) {
        // bila user input 1 maka akan memanggil method tambah()
        // bilangan yg diinput dilakukan konversi ke number (integer)
        let x = await input("Masukkan bilangan 1: ");
        let y = await input("Masukkan bilangan 2: ");
        hasil = tambah(parseInt(x), parseInt(y));
        console.info(`Hasil: ${hasil}`);
      } else if (choice == 2) {
        // bila user input 2 maka akan memanggil method kurang()
        // bilangan yg diinput dilakukan konversi ke number (integer)
        let x = await input("Masukkan bilangan 1: ");
        let y = await input("Masukkan bilangan 2: ");
        hasil = kurang(parseInt(x), parseInt(y));
        console.info(`Hasil: ${hasil}`);
      } else if (choice == 3) {
        // bila user input 3 maka akan memanggil method kali()
        // bilangan yg diinput dilakukan konversi ke number (integer)
        let x = await input("Masukkan bilangan 1: ");
        let y = await input("Masukkan bilangan 2: ");
        hasil = kali(parseInt(x), parseInt(y));
        console.info(`Hasil: ${hasil}`);
      } else if (choice == 4) {
        // bila user input 4 maka akan memanggil method bagi()
        // bilangan yg diinput dilakukan konversi ke float
        let x = await input("Masukkan bilangan 1: ");
        let y = await input("Masukkan bilangan 2: ");
        hasil = bagi(parseFloat(x), parseFloat(y));
        console.info(`Hasil: ${hasil}`);
      } else if (choice == 5) {
        // bila user input 5 maka akan memanggil method akarKuadrat()
        // bilangan yg diinput dilakukan konversi ke number (integer)
        let x = await input("Masukkan bilangan: ");
        hasil = akarKuadrat(parseInt(x));
        console.info(`Hasil: ${hasil}`);
      } else if (choice == 6) {
        let x = await input("Masukkan panjang sisi: ");
        hasil = luasPersegi(parseInt(x));
        // bila user input 6 maka akan memanggil method luasPersegi()
        // bilangan yg diinput dilakukan konversi ke number (integer)
        console.info(`Hasil: ${hasil}`);
      } else if (choice == 7) {
        // bila user input 7 maka akan memanggil method volumeKubus()
        // bilangan yg diinput dilakukan konversi ke number (integer)
        let r = await input("Masukkan panjang rusuk: ");
        hasil = volumeKubus(parseInt(r));
        console.info(`Hasil: ${hasil}`);
      } else if (choice == 8) {
        // bila user input 8 maka akan memanggil method volumeTabung()
        // bilangan yg diinput dilakukan konversi ke float
        let r = await input("Masukkan jari-jari lingkaran: ");
        let t = await input("Masukkan tinggi tabung: ");
        hasil = volumeTabung(parseFloat(r), parseFloat(t));
        console.info(`Hasil: ${hasil}`);
      } else {
        // menghentikan perulangan bila user input 9
        rl.close();
        break;
      }
    }
  } catch (err) {
    // jika ada error dalam operasi maka akan ditangkap disini
    console.info(`Somthing Wrong ${err}`);
  }
}
main();
